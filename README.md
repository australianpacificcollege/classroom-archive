**Google Classroom Archiving Script**
------------------------------------------------------

**Dependencies**

 - Powershell v3.0 or later
 - Google Drive Desktop Application
 - Internet Connection

**Configuration**

In archive.ps1, you will need to change `$basedir` to the folder on your computer where the Google Classroom is synchronised to 

You will also need to change `$archFolder` to the location where the files are being moved to. This can be locally or back onto Google Drive.

*Note: This script has been written purely for archiving assessments 1 and 2 in the gcadmin drive folder called "Classroom". This script may need to be changed to work for other drive folders or other assessments*

**Usage**

Run in Powershell 3.0 or greater