$baseDir = 'C:\Users\Tom\Google Drive\Classroom'
$archFolder = 'C:\Users\Tom\Google Drive\Archive'
$archFolderName = Read-Host -Prompt 'Type the name of the new archive and then press Enter: '
$destination = Join-Path $archFolder $archFolderName

Get-ChildItem $baseDir -directory -Filter 'Assessment 1' -Recurse | ForEach-Object {
    $newPath = Join-Path $destination ($_.FullName -replace [regex]::Escape($baseDir))
    Copy-Item $_.FullName $newPath -Force -Recurse

}

Get-ChildItem $baseDir -directory -Filter 'Assessment 2' -Recurse | ForEach-Object {
    $newPath = Join-Path $destination ($_.FullName -replace [regex]::Escape($baseDir))
    Copy-Item $_.FullName $newPath -Force -Recurse

}

Remove-Item 'C:\Users\Tom\Google Drive\Classroom\*\Assessment 1\*'
Remove-Item 'C:\Users\Tom\Google Drive\Classroom\*\Assessment 2\*'